import kotlin.test.Test
import kotlin.test.assertEquals

class ThingTest {
    @Test
    fun testDoThing() {
        assertEquals("thing", doThing())
    }

    @Test
    fun testDoThingInternal() {
        assertEquals("thing", doInternalThing("thing"))
        assertEquals("broken!", doInternalThing("broken!"))
        assertEquals("", doInternalThing(""))
    }
}